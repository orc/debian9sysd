FROM debian:9
MAINTAINER C. R. Oldham <cro@ncbt.org>

RUN apt -y update && apt -y full-upgrade && apt -y install systemd systemd-container systemd-sysv
CMD ["/sbin/init"]
